var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
// var Bourne = require("bourne");
var Datastore = require('nedb');
var cors = require('cors');

// init express
var app = express();

// set up databases...
var db = { };
db.posts = new Datastore("posts.json");					db.posts.loadDatabase();
db.comments = new Datastore("comments.json");		db.comments.loadDatabase();


// app setup
app.use(express.json());		// not needed ???
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'bower_components')));
// enable CORS
app.use(cors());


// routes:

// get all posts
app.get("/posts", function (req, res, next) {
	db.posts.find({}, function (err, results) {
		res.json(results);
	});
});
// get single post
app.get("/posts/:id", function (req, res, next) {
	db.posts.find({ _id: req.params.id }, function (err, results) {
		res.json(results);
	});
});
// post single post
app.post("/posts", function (req, res, next) {
	console.log(req.body);
	db.posts.insert(req.body, function (err, results) {
		res.json(results);
		console.log(results);
	});
});


// get all comments for post
app.get("/posts/:id/comments", function (req, res, next) {
	db.comments.find({ postId: req.params.id }, function (err, results) {
		res.json(results);
	});
});
// post single comment for post
app.post("/posts/:id/comments", function (req, res, next) {
	db.comments.insert(req.body, function (err, results) {
		res.json(results);
	});
});


// catch alll
app.get('/*', function (req, res, next) {
	db.posts.find({}, function (err, results) {
		res.render("index.ejs", { posts: JSON.stringify(results) });
	});
});



// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});




app.listen(3000);