"use strict";

import Home from "./pages/home.js";

import React from "react";
import Layout from "./layout.js";
import { Router, Route, browserHistory } from "react-router";


const Routes = (
	<Router history={browserHistory}>
		<Route component={Layout}>
			<Route name="home"
				path="/"
				component={Home} />
		</Route>
	</Router>
);

module.exports = Routes;