"use strict";

import React from "react";
import {Link} from 'react-router';


const PostList_Item = React.createClass({
  render(){
    return (
      <li>
				<Link to={'/posts/' + this.props.id }>{this.props.title}</Link>
			</li>
    );
  }
});

export default PostList_Item;
