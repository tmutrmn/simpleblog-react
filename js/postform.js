"use strict";

import React from "react";


class PostForm extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = { 
			title: '',
			content: ''
		};
		
		this.onTitleChange = this.onTitleChange.bind(this);
		this.onContentChange = this.onContentChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
  }
	
  componentDidMount() {
		// let _this = this;
  }
	
	onTitleChange(event) {
		this.setState({
			title: event.target.value 
		});
	}
	
	onContentChange(event) {
		// this.setState({value: event.target.value});
		this.setState({
			content: event.target.value
		});
	}
	
	handleSubmit(event) {
		event.preventDefault();
		console.log("Submitted values are: ", event.target.title.value, event.target.content.value);
		
		let title = this.state.title;
    let content = this.state.content.trim();
		
    this.props.onPostSubmit({ title: title, content: content, pubDate: new Date().toISOString() });		// bubble up to parent
		
    this.setState({		// clear state & form
      title: '',
      content: ''
    });
	}
	
	render() {
		return(
			<form onSubmit={this.handleSubmit}>
				<input type='text' 
					name='title' 
					placeholder='Title' 
					value={this.state.title}
					onChange={this.onTitleChange} />
				<br />
				<textarea name='content' 
					placeholder="Post content here..."
					value={this.state.content}
					onChange={this.onContentChange} />
				<br />
				<button type="submit">&nbsp; Post &nbsp;</button>
			</form>
		)
  }
}

export default PostForm;
