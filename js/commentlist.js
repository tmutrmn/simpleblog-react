"use strict";

import React from "react";
import CommentForm from "./commentform.js";


class CommentList extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = {
			comments:[] 
		};
		this.handleCommentSubmit = this.handleCommentSubmit.bind(this);
  }
	
	componentWillMount() {
		// let _this = this;		// not needed if binding
		$.ajax({
			url: "http://localhost:3000/posts/" + this.props.id + "/comments",
			cache: false
		}).then(function(data) {
			this.setState({
				comments: data
			});
		}.bind(this))		// bind this!
  }
	
  componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
  }
	
	handleCommentSubmit(comment) {
		let _comments = this.state.comments;
		console.log(comment);
/*
		var maxId = 0;
		_comments.map(function(obj){     
			if (obj.id > maxId) maxId = obj.id;    
		});
		comment.id = maxId + 1		// really should get this from the REST call
*/
		$.ajax({
			url: "http://localhost:3000/posts/" + comment.postId + "/comments",
			type: 'POST',
			data: comment
		}).then(function(response) {
			console.log(response);
			comment._id = response._id;		// ass we do
			_comments.push(comment);
			this.setState({ 
				comments: _comments
			})
		}.bind(this))	
	}
	
	render() {
		return(
			<div>
				<h3>comments:</h3>
				<ul>
					{
						this.state.comments.length ?
						this.state.comments.map(comment=><li key={comment._id}><hr /><p><strong>{comment.name}</strong> said on {comment.date}: </p><p>{comment.text}</p></li>)
						: <li>No comments. Be first to comment...</li>
					}
				</ul>
				<CommentForm key='comment-form' id={this.props.id} onCommentSubmit={this.handleCommentSubmit} />
			</div>
		)
  }
}

export default CommentList;
