"use strict";

import React from "react";
import PostList from "./postlist.js";


const Home = React.createClass({
  render(){
    return (
			<div>
				<h1>My Blog</h1>
				<PostList />
			</div>
    );
  }
});

export default Home;
