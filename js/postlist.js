"use strict";

import React from "react";
import {Link} from 'react-router';
import PostList_Item from "./postlist_item.js";
import PostForm from "./postform.js";


class PostList extends React.Component {
	constructor() {
		super();
		this.state = { items:[] };
		this.handlePostSubmit = this.handlePostSubmit.bind(this);
  }

	componentWillMount() {
		let _this = this;

		$.ajax({
			url: "http://localhost:3000/posts"
		}).then(function(data) {
			_this.setState({
				items: data
			});
		})
  }
	
  componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}	

	
	handlePostSubmit(post) {
		let _posts = this.state.items;

		$.ajax({
			url: "http://localhost:3000/posts/",
			type: 'POST',
			data: post
		}).then(function(response) {
			post._id = response._id;		// set the id
			_posts.push(post);
			this.setState({ 
				items: _posts
			})
		}.bind(this))	

	}
	
	
	// <Postlist_item {...this.props} />
	// <div>{React.cloneElement(this.props.children, {...this.props})}</div>
	// { React.cloneElement(this.props.children, this.state) } //
	render() {
		return(
			<div>
				<ul>
					{ 
						this.state.items.length ? this.state.items.map(item => <PostList_Item key={item._id} id={item._id} title={item.title} />) : <li>Loading...</li>
					}
				</ul>
				<PostForm key='post-form' onPostSubmit={this.handlePostSubmit} />
				<p><Link to={'/about'}>About</Link></p>
			</div>
		)
  }
}

export default PostList;
