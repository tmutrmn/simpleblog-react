"use strict";

import React from "react";
import { Link } from "react-router";
import CommentForm from "./commentform.js";
import CommentList from "./commentlist.js";


class Post extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			id: this.props.routeParams.id,
			data: {}
    }
  }
	
	componentWillMount() {
		let _this = this;
		$.ajax({
			url: "http://localhost:3000/posts/" + this.state.id + "",
			cache: false
		}).then(function(data) {
			_this.setState({
				data: data[0]
			});
		})
  }
	
  componentDidMount() {
		// never run setState here because it will trigger an endless update loop...
	}
		
	
	render() {		// shouild use moment.js for formatting date data...
		return(
			<div>
				<h1>{this.state.data.title}</h1>
				<h4>{this.state.data.pubDate}</h4>
				<p>{this.state.data.content}</p>
				<CommentList key={this.state.id} id={this.state.id} />
				<br />
				<Link to='/'>&laquo; back</Link>
			</div>
		)
  }
}

export default Post;
