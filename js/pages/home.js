"use strict";
import React from "react";


const Home = React.createClass({
  displayName: "Home page",
  
	componentDidMount(){
    console.log(this.props)
  },
	
  render(){
    return (
			<div>
				<h1>My Blog</h1>
				<Postlist />
			</div>
    );
  }

});

module.exports = Home;
