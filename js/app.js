"use strict";

/*Hello World!*/
import React from "react";
import { render } from "react-dom";
import { Router, Route, Link, browserHistory } from "react-router";

import Home from "./home.js";
import Post from "./post.js";


const Hello = () => <h1>Hello World!</h1>

const NotFound = () => (
	<h1>404.. This page is not found!</h1>
)

const Address = () => (
	<h2>Address</h2>
)

const About = (props) => (
	<div>
		<h1>About</h1>
		<p>Simple blog posting / commenting app <br />just to get familiar with REACT.js. <br /> Node.js/Express backend with NeDB</p>
		<p>&copy; Teemu T. 2016</p>
		<Link to='/'>&laquo; back</Link>
	</div>
)


const Routes = (
	<Router history={browserHistory}>
		<Route name="home" path="/" component={Home} />
		<Route name="post" path="/posts/:id" component={Post} />
		<Route name="hello" path="/hello" component={Hello} />
		<Route name="about" path='/about(/:name)' component={About} />
		<Route name="address" path='/address' component={Address} />
		<Route path='*' component={NotFound} />
	</Router>
);



render(
	Routes,
  document.getElementById("react")
)
