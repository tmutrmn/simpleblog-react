"use strict";

import React from "react";


class CommentForm extends React.Component {
	
	constructor(props) {
		super(props);
		this.state = { 
			name: '',
			comment: ''
		};
		
		this.onNameChange = this.onNameChange.bind(this);
		this.onCommentChange = this.onCommentChange.bind(this);
		this.handleSubmit = this.handleSubmit.bind(this);
  }
	
  componentDidMount() {
		// let _this = this;
  }
	
	onNameChange(event) {
		this.setState({
			name: event.target.value 
		});
	}
	
	onCommentChange(event) {
		//this.setState({value: event.target.value});
		this.setState({
			comment: event.target.value
		});
	}
	
	handleSubmit(event) {
		event.preventDefault();
		// console.log("Submitted values are: ", event.target.name.value, event.target.comment.value);
		
		let name = this.state.name;
    let comment = this.state.comment.trim();
		
    this.props.onCommentSubmit({ name: name, text: comment, postId: this.props.id, date: new Date().toISOString() });		// bubble up to parent
		
    this.setState({		// clear state & form
      name: '',
      comment: ''
    });
	}
	
	render() {
		return(
			<form onSubmit={this.handleSubmit}>
				<input type='text' 
					name='name' 
					placeholder='name' 
					value={this.state.name}
					onChange={this.onNameChange} />
				<br />
				<textarea name='comment' 
					placeholder="Your comment here..."
					value={this.state.comment}
					onChange={this.onCommentChange} />
				<br />
				<button type="submit">Submit</button>
			</form>
		)
  }
}

export default CommentForm;
