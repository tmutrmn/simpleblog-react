# SimpleBlog

## Node/Express Backend which implements RESTful service and outputs JSON data for the frontend

## REACT (reactpack) JS/ES6 Frontend that consumes JSON data fetched from backend (via ajax calls)
 
#### Made with Node/Express + REACT (reactpack). jQuery is used for the AJAX calls (poor choice - i know)


## 1. Install node.js and npm:

https://nodejs.org/en/

select proper installation files for you Oporating Shystem

## 2. GIT clone repo from:

[https://gitlab.com/tmutrmn/simpleblog-react](https://gitlab.com/tmutrmn/simpleblog-react)

## 3. Install dependencies:

Navigate to the cloned repo’s directory.

Run:

```npm install``` 

## 4. Run the BACKEND server:

Open command prompt/bash/bosh if it's not already open. (this will be bosh no:1)
In the project directory run:

```node server.js``` 

This will start the Backend server, and now you can see JSON output if you point your Browser/Postman/Curl to (for exemple):

http://localhost:3000/posts

http://localhost:3000/posts/3FdPczB8PfRJzxXE

http://localhost:3000/posts/3FdPczB8PfRJzxXE/comments


## 5. Run the FRONTEND server:

Open another command prompt/bash/bosh if it's not already open. (this will be bosh no:2)
in the project directory run:

```npm start``` 

This will start the Frontend client.
Point your browser to 

http://localhost:8080/

and you'll see an REACT JS frontend app that uses data from the aforementioned Backend server.
